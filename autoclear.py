import os
import shutil
import logging
import configuration as conf
from datetime import datetime


class Application:

    def __init__(self, conf_file):
        self.name = conf_file['name']
        self.path = conf_file['path']
        self.projects = []

    def add_project(self, obj):
        self.projects.append(obj)


class Project:

    def __init__(self, dir, project_name):

        self.dir = dir
        self.version_count = 0
        self.project_name = project_name.split('/')[-1]
        self.versions = []

    def add_version(self, obj):
        self.versions.append(obj)
        self.version_count += 1

    def get_versions(self):
        versions = []
        for v in self.versions:
            versions.append(v.version_name)
        return versions

    def get_version(self, name):
        for v in self.versions:
            if name == v.version_name:
                return v

    def get_old_versions(self):
        old_version = []
        limit = self.version_count - conf.STORAGE_VERSIONS
        add_version = 0
        self.versions.sort(key=lambda x:x.date)
        for v in self.versions:
            if (datetime.now() - v.date).days >= conf.STORAGE_DAY:
                if add_version < limit:
                    old_version.append(v)
                    add_version += 1
        return old_version


class Version:

    def __init__(self, version_path, version_name):

        self.version_name = version_name
        self.version_path = version_path
        self.date = 0
        self.version_obj = []

    def get_change_date(self, version_obj, version_path):
        uts = os.path.getctime(version_path+'/'+version_obj)
        date = datetime.utcfromtimestamp(uts)
        self.date = date

    def add_obj(self, obj):
        self.version_obj.append(obj)
        self.get_change_date(obj, self.version_path)


def get_files_list():
    logging.debug('Get list files')
    apps = []
    conf_data = conf.FILES_DATA
    for c in conf_data:
        path = c['path']
        type_files = c['type_files']
        app = Application(c)
        for dirname, dirnames, filenames in os.walk(path, topdown=True):
            dirnames[:] = [d for d in dirnames if d not in conf.EXCLUDE_FOLDERS]
            logging.debug(('Scan directory: {0}').format(dirname))
            for file in os.listdir(dirname):
                if file == conf.NEXUS_DETECT_FILE:
                    project = Project(dirname, dirname)
                    for dir in dirnames:
                        version = Version(dirname, dir)
                        version.add_obj(dir)
                        project.add_version(version)
                    app.add_project(project)
                elif file.split('.')[-1] in type_files:
                    project = Project(dirname, dirname)
                    for _file in os.listdir(dirname):
                        name = _file.split('.')[0]
                        if name not in project.get_versions():
                            version = Version(dirname, name)
                            version.add_obj(_file)
                            project.add_version(version)
                        else:
                            version = project.get_version(name)
                            version.add_obj(_file)
                    app.add_project(project)
                    break

        apps.append(app)

    return apps


def delete_item(item):
    try:
        if os.path.isfile(item):
            # os.remove(item)
            logging.info(('Delete file {0}').format(item))
        elif os.path.isdir(item):
            #shutil.rmtree(item)
            logging.info(('Delete folder {0}').format(item))
    except OSError as e:
        logging.error(('Can`t deleted {0} cause: {1}').format(item, e))


def main():
    logging.info(('Script start {0}').format(datetime.now()))
    apps = get_files_list()

    for app in apps:

        for project in app.projects:
            v = project.get_old_versions()
            for obj in v:
                for file in obj.version_obj:
                    delete_item(obj.version_path+'/'+file)

    logging.info(('Script end {0}').format(datetime.now()))


if __name__ == "__main__":
    logging.basicConfig(format='%(levelname)-8s [%(asctime)s] %(message)s',
                        level=logging.DEBUG,
                        filename='autoclear.log')

    main()

